# Cleevio Backend Developer Task
The task was to implement backend for uploading products for e-shop with watches. 
## Uploading
Uploading is done via HTTP request with Post method on url ending with /watches. This request is handled in WatchController which calls WatchService to validate the request data and communicates with WatchRepository to store data to database.
## Request format change
As of now the application is prepared for handling requests in JSON and XML format which is determined in header field called format. Other formats can be added using the getMapper function in WatchService by adding new case to prepared switch which returns ObjectMapper instance able to handle desired format.
## Suggested additions
WatchController can handle more HTTP requests which should be done via implementing new methods with corresponding HTTP method mapping annotation e.g. Get method to retrieve all watches stored in database should done via implementing method called getWatches with @GetMapping annotation. WatchService than needs to implement method for handling business logic.
