package com.cleevio.recrutiment.osp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.cleevio.recrutiment.osp.service.WatchService;

@RestController
@CrossOrigin(origins = "http://localhost:9090")
public class WatchController
{
	@Autowired
	WatchService watchService;

	@PostMapping("/watches")
	public ResponseEntity<String> createWatch(@RequestBody String watchData, @RequestHeader("format") String format)
	{
		try{
			watchService.createWatch(watchData, format);
		}
		catch(Exception e){
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Watch is created successfully", HttpStatus.CREATED);
	}
}
