package com.cleevio.recrutiment.osp.service;

import java.nio.charset.StandardCharsets;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;
import com.cleevio.recrutiment.osp.WatchRepository;
import com.cleevio.recrutiment.osp.model.Watch;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@Service
public class WatchService implements IWatchService
{

	private WatchRepository watchRepository;
	public WatchService(WatchRepository watchRepository)
	{
		this.watchRepository = watchRepository;
	}

	@Override
	public void createWatch(String watchData, String format) throws Exception
	{
		ObjectMapper objectMapper = getMapper(format);
		if (objectMapper == null)
		{
			throw new Exception("Invalid format");
		}

		Watch watch = objectMapper.readValue(watchData, Watch.class);
		if(watch.getTitle() == null || watch.getTitle().isEmpty())
		{
			throw new Exception("Empty watch title");
		}

		if(!Base64.isBase64(watch.getFountain()))
		{
			throw new Exception("Watch fountain is not in base64 encoding");
		}

		watchRepository.save(watch);
	}


	public ObjectMapper getMapper(String format)
	{
		switch (format) {
			case "JSON":
				return new ObjectMapper();
			case "XML":
				return new XmlMapper();
			default:
				return null;
		}
	}

}
