package com.cleevio.recrutiment.osp.service;

import com.cleevio.recrutiment.osp.model.Watch;

public interface IWatchService
{
	public abstract void createWatch(String watchData, String format) throws Exception;
}
