package com.cleevio.recrutiment.osp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.cleevio.recrutiment.osp.model.Watch;

@Repository
public interface WatchRepository extends CrudRepository<Watch, Long>
{
}
